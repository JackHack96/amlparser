#ifndef AMLPARSER_AMLDOMPARSER_H
#define AMLPARSER_AMLDOMPARSER_H

#include "AMLConstants.h"
#include "AMLDOMError.h"
#include "AMLIO.h"
#include "XercesString.h"
#include <fstream>
#include <stack>
#include <xercesc/dom/DOM.hpp>

XERCES_CPP_NAMESPACE_USE

/**
 * This is the main AutomationML DOM parser class.
 * It handles the parsing of the AML file, and offers some useful methods for
 * extracting information.
 */
class AMLDOMParser
{
  public:
    // ---------------------------------------
    // Constructor and destructor
    // ---------------------------------------
    /**
   * Instantiate an AML DOM parser with default parameters
   * @param fileName AML file path
   */
    explicit AMLDOMParser();

    /**
   * Destructor method
   */
    ~AMLDOMParser();

    /**
   * Launch the actual parsing process
   * @param fileName Path of the AML file to parse
   */
    void parseDocument(const std::string &fileName);

    // ---------------------------------------
    // Getters
    // ---------------------------------------
    /**
   * Get a std::vector of the SystemUnits found in the AML file
   * @return List of SystemUnits
   */
    const std::vector<SystemUnit> &getSystemUnits() const;

    /**
   * Get a std::vector of the InternalElements found in the AML file
   * @return List of InternalElements
   */
    const std::vector<InternalElement> &getInternalElements() const;

    // ---------------------------------------
    // Utility functions
    // ---------------------------------------
    /**
   * Recursively search in the AML file for the given InternalElement's ID
   * @param id ID of the wanted SystemUnit
   * @return Pointer to SystemUnit if found
   */
    const InternalElement *getInternalElementById(const XercesString &id);

    /**
   * Get the root XML node of the AML file
   * @return DOMDocument referring to root node
   */
    const DOMDocument *getDOMDocument();

  private:
    DOMImplementation *impl;
    DOMLSParser       *parser;
    DOMConfiguration  *config;
    AMLDOMErrorHandler errorHandler;

    // root node
    DOMDocument *doc;

    // node lists of AML document
    std::vector<SystemUnit>      systemUnits;
    std::vector<InternalElement> internalElements;

    // Populating the structures
    void parseSystemUnits();

    void parseInstanceHierarchy();

    void parseIE(const DOMElement &element, InternalElement *internalElement);

    static void parseIEExternalInterface(const DOMElement &element, ExternalInterface *externalInterface);

    static void parseIEAttribute(DOMElement *element, InternalElement *internalElement);

    // Utility functions
    static const InternalElement *
    getInternalElementById(const std::vector<InternalElement> &internalElementList, const XercesString &id);
};

#endif // AMLPARSER_AMLDOMPARSER_H
