#ifndef AMLPARSER_AMLDOMERROR_H
#define AMLPARSER_AMLDOMERROR_H

#include "XercesString.h"
#include <xercesc/dom/DOM.hpp>

XERCES_CPP_NAMESPACE_USE

class AMLDOMErrorHandler : DOMErrorHandler
{
  public:
     AMLDOMErrorHandler();
    ~AMLDOMErrorHandler();

    bool getSawErrors() const;

    bool handleError(const DOMError &) override;
    void resetErrors();

  private:
    bool sawErrors;
};

#endif // AMLPARSER_AMLDOMERROR_H
