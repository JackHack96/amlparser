#ifndef XERCESSTRING_H
#define XERCESSTRING_H

#include <iostream>
#include <memory>
#include <xercesc/util/XMLString.hpp>

/*
 * These are simple methods that lets us do easy transcoding of XMLCh data
 * to local code page for display
 */
using XercesString = std::basic_string<XMLCh>;

/**
 * Convert from native encoding to wide character encoding (i.e. UTF-8)
 * @param str Native character string
 * @return Wide character encoded string
 */
XercesString fromNative(const char *str);

/**
 * Convert from native std::string to wide character encoding (i.e. UTF-8)
 * @param str Native character string
 * @return Wide character encoded string
 */
XercesString fromNative(const std::string &str);

/**
 * Convert from wide character encoding to native encoding
 * @param str Wide character encoded string
 * @return Native std::string
 */
std::string toNative(const XMLCh *str);

/**
 * Convert from wide character encoding to native encoding
 * @param str Wide character encoded string
 * @return Native std::string
 */
std::string toNative(const XercesString &str);

// overriding the << operator so I can print it with cout
inline std::ostream &operator<<(std::ostream &target, const XercesString &toDump)
{
    target << toNative(toDump);
    return target;
}

#endif // XERCESSTRING_H