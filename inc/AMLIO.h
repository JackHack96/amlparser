#ifndef AMLPARSER_AMLIO_H
#define AMLPARSER_AMLIO_H

#include "XercesString.h"
#include <map>
#include <string>
#include <utility>
#include <vector>

/**
 * This struct represents a single I/O port
 * In AML it's equivalent to an ExternalInterface
 */
struct ExternalInterface
{
    XercesString name;
    XercesString id;
    XercesString direction;
    XercesString minCardinality;
    XercesString maxCardinality;
    XercesString simulinkName;
};

/**
 * SystemUnit structure, representing the external
 * interfaces of the machine and it's I/O ports
 */
struct SystemUnit
{
    XercesString                   name;
    XercesString                   id;
    std::vector<ExternalInterface> extnInterfaces;
};

/**
 * This struct represents the instance of a system
 * unit, with its ID and its internal links
 */
struct InternalElement
{
    XercesString                              name;
    XercesString                              id;
    XercesString                              refBaseRoleClassPath;
    std::vector<InternalElement>              internalElements;
    std::vector<ExternalInterface>            extnInterfaces;
    std::multimap<XercesString, XercesString> internalLinks;
    std::map<XercesString, XercesString>      attributeList;
};

#endif // AMLPARSER_AMLIO_H
