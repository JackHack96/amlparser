#ifndef AMLPARSER_AMLCONSTANTS_H
#define AMLPARSER_AMLCONSTANTS_H

#include <xercesc/util/XMLString.hpp>

/*
 * These are the constants being used inside the parser.
 * We use this technique because XMLCh::transcode costs more in terms of
 * performance and memory, as it's used for dynamic strings. By making constant
 * strings, we can optimize at compile-time.
 */
static const XMLCh AMLInstanceHierarchy[]    = u"InstanceHierarchy";
static const XMLCh AMLInternalElement[]      = u"InternalElement";
static const XMLCh AMLExternalInterface[]    = u"ExternalInterface";
static const XMLCh AMLRoleRequirements[]     = u"RoleRequirements";
static const XMLCh AMLName[]                 = u"Name";
static const XMLCh AMLID[]                   = u"ID";
static const XMLCh AMLInternalLink[]         = u"InternalLink";

static const XMLCh AMLRefPartnerSideA[]      = u"RefPartnerSideA";
static const XMLCh AMLRefPartnerSideB[]      = u"RefPartnerSideB";

static const XMLCh AMLAttribute[]            = u"Attribute";
static const XMLCh AMLDirection[]            = u"Direction";
static const XMLCh AMLCardinality[]          = u"Cardinality";
static const XMLCh AMLCategory[]             = u"Category";
static const XMLCh AMLValue[]                = u"Value";
static const XMLCh AMLRefBaseRoleClassPath[] = u"RefBaseRoleClassPath";
static const XMLCh AMLSimulinkName[]         = u"SimulinkName";

static const XMLCh AMLMinOccur[]             = u"MinOccur";
static const XMLCh AMLMaxOccur[]             = u"MaxOccur";

#endif // AMLPARSER_AMLCONSTANTS_H
