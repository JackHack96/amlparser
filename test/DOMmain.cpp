#include "AMLDOMParser.h"
#include <chrono>
#include <iostream>

AMLDOMParser parser;

void usage() {
  std::cout << "\nUsage:\n"
               "AMLDOMParser [AML_FILE].aml"
            << std::endl;
}

void printInternalElements(const std::vector<InternalElement> &ie) {
  for (auto &s : ie) {
    std::cout << s.name << ", " << s.id << std::endl;
    for (auto &e : s.extnInterfaces) {
      std::cout << " - " << e.name << ", " << e.id << ", " << e.direction << std::endl;
      if (!e.minCardinality.empty() && !e.maxCardinality.empty())
        std::cout << "   with minimum cardinality " << e.minCardinality << ", maximum " << e.maxCardinality << std::endl;
    }
    for (auto &e : s.internalLinks) {
      std::cout << e.first << " -> " << e.second << std::endl;
    }
    if (!s.internalElements.empty())
      printInternalElements(s.internalElements);
  }
}

void printLinkedPorts(const std::vector<InternalElement> &ie) {
  for (auto &a : ie) {
    if (!a.internalLinks.empty())
      std::cout << a.name << std::endl;
    for (auto &b : a.internalLinks) {
      XercesString sourceName = b.first.substr(b.first.find(u":") + 1, b.first.length());
      XercesString sourceID = b.first.substr(0, b.first.find(u":"));
      XercesString destName = b.second.substr(b.second.find(u":") + 1, b.second.length());
      XercesString destID = b.second.substr(0, b.second.find(u":"));
      std::cout << "\tPort " << sourceName << " of " << parser.getInternalElementById(sourceID)->name << " is connected to port " << destName << " of ";
      auto c = parser.getInternalElementById(destID);
      if (c != nullptr)
        std::cout << c->name << std::endl;
      else
        std::cout << "unknow port <--- !!!ERROR!!!" << std::endl;
    }
    if (!a.internalElements.empty())
      printLinkedPorts(a.internalElements);
  }
}

int main(int argC, char *argV[]) {
  // Check command line and extract arguments.
  if (argC < 2) {
    usage();
    return 1;
  }

  auto start = std::chrono::steady_clock::now();
  parser.parseDocument(argV[1]);
  auto end = std::chrono::steady_clock::now();

  std::cout << "Found the following internal elements: " << std::endl;
  printInternalElements(parser.getInternalElements());

  std::cout << "=============================" << std::endl;
  std::cout << "Here is how ports are linked" << std::endl;
  printLinkedPorts(parser.getInternalElements());

  std::cout << "=============================" << std::endl;
  std::cout << "Total parsing time : " << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << " ms" << std::endl;

  return 0;
}