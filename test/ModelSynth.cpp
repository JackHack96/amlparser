#include "AML2MATLAB.h"
#include "AMLDOMParser.h"

void usage() {
  std::cout << "\nUsage:\n"
               "ModelSynthesis [AML_FILE].aml [MATLAB_SCRIPT].m"
            << std::endl;
}

int main(int argC, char *argV[]) {
  // Check command line and extract arguments.
  if (argC < 3) {
    usage();
    return 1;
  }

  AMLDOMParser parser;
  parser.parseDocument(argV[1]);
  AML2MATLAB converter(&parser, argV[2]);
  converter.convertToMATLAB();

  return 0;
}