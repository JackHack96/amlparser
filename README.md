# AMLParser

[![pipeline status](https://gitlab.com/jackhack96/amlparser/badges/master/pipeline.svg)](https://gitlab.com/jackhack96/amlparser/commits/master)

This is an AutomationML parser based on Xerces-C using DOM. It fully validates the AML document against the correspondent CAEX schema.

Currently, it handles the InstanceHierarchy and the SystemUnits.