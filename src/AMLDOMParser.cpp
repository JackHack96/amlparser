#include "AMLDOMParser.h"

AMLDOMParser::AMLDOMParser()
{
    // initialize the Xerces environment
    XMLPlatformUtils::Initialize();

    static const XMLCh gLS[] = {chLatin_L, chLatin_S, chNull};
    impl                     = DOMImplementationRegistry::getDOMImplementation(gLS);
    parser = ((DOMImplementationLS *) impl)->createLSParser(DOMImplementationLS::MODE_SYNCHRONOUS, nullptr);
    config = parser->getDomConfig();
    // turning on full validation
    // the following parameters' documentation can be found in
    // the DOM examples provided with Xerces-C
    config->setParameter(XMLUni::fgDOMNamespaces, true);
    config->setParameter(XMLUni::fgXercesSchema, true);
    config->setParameter(XMLUni::fgXercesHandleMultipleImports, true);
    config->setParameter(XMLUni::fgXercesSchemaFullChecking, true);
    config->setParameter(XMLUni::fgDOMValidate, true);
    config->setParameter(XMLUni::fgDOMDatatypeNormalization, true);
    config->setParameter(XMLUni::fgDOMElementContentWhitespace, false);
    // attach the error handler
    config->setParameter(XMLUni::fgDOMErrorHandler, &errorHandler);
}

AMLDOMParser::~AMLDOMParser()
{
    parser->release();
    XMLPlatformUtils::Terminate();
}

// ---------------------------------------
// Getters
// ---------------------------------------
const DOMDocument *AMLDOMParser::getDOMDocument()
{
    return doc;
}

const std::vector<SystemUnit> &AMLDOMParser::getSystemUnits() const
{
    return systemUnits;
}

const std::vector<InternalElement> &AMLDOMParser::getInternalElements() const
{
    return internalElements;
}

// ---------------------------------------
// Various methods
// ---------------------------------------
void AMLDOMParser::parseDocument(const std::string &fileName)
{
    // check if file exists
    std::ifstream f(fileName);
    if (!f.good())
        throw std::runtime_error("Could not open AML file");

    errorHandler.resetErrors();
    try
    {
        parser->resetDocumentPool();
        doc = parser->parseURI(fileName.c_str());

        // parseSystemUnits();
        parseInstanceHierarchy();
    }
    catch (const XMLException &toCatch)
    {
        std::cerr << "\nError during parsing: '" << fileName << "'\n"
                  << "Exception message is:  \n"
                  << toNative(toCatch.getMessage()) << "\n"
                  << std::endl;
    }
    catch (const DOMException &toCatch)
    {
        const unsigned int maxChars = 2047;
        XMLCh              errText[maxChars + 1];

        std::cerr << "\nDOM Error during parsing: '" << fileName << "'\n"
                  << "DOMException code is:  " << toCatch.code << std::endl;

        if (DOMImplementation::loadDOMExceptionMsg(toCatch.code, errText, maxChars))
            std::cerr << "Message is: " << toNative(errText) << std::endl;
    }
    catch (...)
    {
        std::cerr << "\nUnexpected exception during parsing: '" << fileName << "'\n";
    }
}

void AMLDOMParser::parseSystemUnits()
{
    // we first create our list of SystemUnitClass tags, then
    // we'll iterate through it
    //  DOMNodeList *systemUnitClass =
    //  doc->getElementsByTagName(XMLString::transcode("SystemUnitClass"));
    //
    //  for (int i = 0; i < systemUnitClass->getLength(); i++)
    //  {
    //    DOMNode    *systemUnit = systemUnitClass->item(i);
    //    // we are sure it contains Name and ID given that
    //    it's a list of only SystemUnitClass tags SystemUnit
    //    tmpSysUnit{
    //        systemUnit->getAttributes()->getNamedItem(XMLString::transcode("Name"))->getNodeValue(),
    //        systemUnit->getAttributes()->getNamedItem(XMLString::transcode("ID"))->getNodeValue()
    //    };
    //
    //    // for each SystemUnitClass, we have to parse the
    //    ExternalInferfaces for (DOMNode
    //    *interface(systemUnit->getFirstChild());
    //         interface != nullptr;
    //         interface = interface->getNextSibling()) {
    //      // not sure all the nodes are ExternalInterface
    //      if (StrX::compare(interface->getNodeName(),
    //      "ExternalInterface")) {
    //        ExternalInterface tmpExtnInterface{
    //            XMLString::transcode(
    //                interface->getAttributes()->getNamedItem(XMLString::transcode("Name"))->getNodeValue()),
    //            XMLString::transcode(interface->getAttributes()->getNamedItem(XMLString::transcode("ID"))->getNodeValue())
    //        };
    //
    //        // for each ExternalInterface, we have to
    //        eventually parse the Attributes for (DOMNode
    //        *attribute(interface->getFirstChild());
    //             attribute != nullptr;
    //             attribute = attribute->getNextSibling()) {
    //          // values inside the Attribute tag can vary,
    //          depending on the attribute itself const XMLCh
    //          *attributeName =
    //          attribute->getAttributes()->getNamedItem(
    //              XMLString::transcode("Name"))->getNodeValue();
    //
    //          if (StrX::compare(attributeName, "Direction"))
    //          {
    //            for (DOMNode
    //            *value(attribute->getFirstChild());
    //                 value != nullptr;
    //                 value = value->getNextSibling()) {
    //              if (StrX::compare(value->getNodeName(),
    //              "Value")) {
    //                char *cstr = XMLString::transcode(
    //                    value->getFirstChild()->getNodeValue());
    //                    // i know it's ugly but it's XML
    //                    anyway
    //                tmpExtnInterface.direction = cstr;
    //                XMLString::release(&cstr);
    //              }
    //            }
    //          } else if (StrX::compare(attributeName,
    //          "Cardinality")) {
    //            //TODO: decidere cosa fare qui
    //          } else if (StrX::compare(attributeName,
    //          "Category")) {
    //            //TODO: decidere cosa fare qui
    //          }
    //        }
    //        tmpSysUnit.extnInterfaces.push_back(tmpExtnInterface);
    //      }
    //    }
    //    AMLDOMParser::systemUnits.push_back(tmpSysUnit);
    //  }
}

void AMLDOMParser::parseInstanceHierarchy()
{
    // we first create our list of InstanceHierarchy tags,
    // then we'll iterate through it
    auto *instanceHierarchyList = doc->getElementsByTagName(AMLInstanceHierarchy);

    for (int i = 0; i < instanceHierarchyList->getLength(); i++)
    {
        auto *internalElementList = dynamic_cast<DOMElement *>(instanceHierarchyList->item(i))->getChildNodes();

        for (int j = 0; j < internalElementList->getLength(); j++)
        {
            auto *internalElementEntry = dynamic_cast<DOMElement *>(internalElementList->item(j));
            if (XMLString::compareIString(internalElementEntry->getTagName(), AMLInternalElement) == 0)
            {
                InternalElement tmpInternalElement{
                    internalElementEntry->getAttributeNode(AMLName)->getValue(),
                    internalElementEntry->getAttributeNode(AMLID)->getValue()};
                parseIE(*internalElementEntry, &tmpInternalElement);
                AMLDOMParser::internalElements.push_back(tmpInternalElement);
            }
        }
    }
}

void AMLDOMParser::parseIE(const DOMElement &element, InternalElement *internalElement)
{
    auto *childNodes = element.getChildNodes();
    for (int i = 0; i < childNodes->getLength(); i++)
    {
        auto *c = dynamic_cast<DOMElement *>(childNodes->item(i));

        // if the current child node is another InternalElement,
        // recursively call parseIE() on it
        if (XMLString::compareIString(c->getTagName(), AMLInternalElement) == 0)
        {
            InternalElement tmpInternalElement{
                c->getAttributeNode(AMLName)->getValue(),
                c->getAttributeNode(AMLID)->getValue()};
            parseIE(*c, &tmpInternalElement);
            internalElement->internalElements.push_back(tmpInternalElement);
        }
        // if it's an ExternalInterface, call
        // parseIEExternalInterface() on it
        else if (XMLString::compareIString(c->getTagName(), AMLExternalInterface) == 0)
        {
            ExternalInterface tmpExtnInterface{
                c->getAttributeNode(AMLName)->getValue(),
                c->getAttributeNode(AMLID)->getValue()};
            parseIEExternalInterface(*c, &tmpExtnInterface);
            internalElement->extnInterfaces.push_back(tmpExtnInterface);
        }
        // if it's an InternalLink just add it to the current
        // internalElement
        else if (XMLString::compareIString(c->getTagName(), AMLInternalLink) == 0)
        {
            internalElement->internalLinks.insert(std::pair<XercesString, XercesString>(
                c->getAttribute(AMLRefPartnerSideA),
                c->getAttribute(AMLRefPartnerSideB)));
        }
        // if it's a RoleRequirements tag, we just save the
        // reference as a string
        else if (XMLString::compareIString(c->getTagName(), AMLRoleRequirements) == 0)
        {
            internalElement->refBaseRoleClassPath = c->getAttribute(AMLRefBaseRoleClassPath);
        }
        // if it's an Attribute tag, call parseIEAttribute() on
        // it
        else if (XMLString::compareIString(c->getTagName(), AMLAttribute) == 0)
        {
            parseIEAttribute(c, internalElement);
        }
    }
}

void AMLDOMParser::parseIEExternalInterface(const DOMElement &element, ExternalInterface *externalInterface)
{
    // for each ExternalInterface, we have to eventually parse
    // the Attributes
    auto *attributeList = element.getChildNodes();
    for (int k = 0; k < attributeList->getLength(); k++)
    {
        // values inside the Attribute tag can vary, depending
        // on the attribute itself
        auto *attributeEntry = dynamic_cast<DOMElement *>(attributeList->item(k));

        if (XMLString::compareIString(attributeEntry->getTagName(), AMLAttribute) == 0)
        {
            if (XMLString::compareIString(attributeEntry->getAttribute(AMLName), AMLDirection) == 0)
            {
                if (attributeEntry->getElementsByTagName(AMLValue)->getLength() > 0)
                    // the direction is expressed inside the Value tag
                    externalInterface->direction =
                        attributeEntry->getElementsByTagName(AMLValue)->item(0)->getFirstChild()->getNodeValue();
            }
            else if (XMLString::compareIString(attributeEntry->getAttribute(AMLName), AMLCardinality) == 0)
            {
                auto *cardinalityList = attributeEntry->getChildNodes();

                for (int l = 0; l < cardinalityList->getLength(); l++)
                {
                    auto *cardinalityEntry = dynamic_cast<DOMElement *>(cardinalityList->item(l));
                    if (XMLString::compareIString(cardinalityEntry->getAttribute(AMLName), AMLMinOccur) == 0)
                    {
                        if (cardinalityEntry->getElementsByTagName(AMLValue)->getLength() >
                            0) // checking if the Value tag is expressed
                            externalInterface->minCardinality = cardinalityEntry->getElementsByTagName(AMLValue)
                                                                    ->item(0)
                                                                    ->getFirstChild()
                                                                    ->getNodeValue();
                    }
                    if (XMLString::compareIString(cardinalityEntry->getAttribute(AMLName), AMLMaxOccur) == 0)
                    {
                        if (cardinalityEntry->getElementsByTagName(AMLValue)->getLength() > 0)
                            externalInterface->maxCardinality = cardinalityEntry->getElementsByTagName(AMLValue)
                                                                    ->item(0)
                                                                    ->getFirstChild()
                                                                    ->getNodeValue();
                    }
                }
            }
            else if (XMLString::compareIString(attributeEntry->getAttribute(AMLName), AMLSimulinkName) == 0)
            {
                externalInterface->simulinkName =
                    attributeEntry->getElementsByTagName(AMLValue)->item(0)->getFirstChild()->getNodeValue();
            }
        }
    }
}

void AMLDOMParser::parseIEAttribute(DOMElement *element, InternalElement *internalElement)
{
    if (element->hasChildNodes())
    {
        auto *childNodes = element->getChildNodes();
        for (int i = 0; i < childNodes->getLength(); i++)
        {
            auto *c = dynamic_cast<DOMElement *>(childNodes->item(i));

            if (XMLString::compareIString(c->getTagName(), AMLValue) == 0)
                internalElement->attributeList.insert(std::pair<XercesString, XercesString>(
                    element->getAttribute(AMLName),
                    c->getFirstChild()->getNodeValue()));
            else if (XMLString::compareIString(c->getTagName(), AMLAttribute) == 0)
                parseIEAttribute(c, internalElement);
        }
    }
}

const InternalElement *AMLDOMParser::getInternalElementById(const XercesString &id)
{
    return getInternalElementById(internalElements, id);
}

const InternalElement *
AMLDOMParser::getInternalElementById(const std::vector<InternalElement> &internalElementList, const XercesString &id)
{
    for (auto &ie : internalElementList)
    {
        if (ie.id == id)
            return &ie;
        auto res = getInternalElementById(ie.internalElements, id);
        if (res != nullptr)
            return res;
    }
    return nullptr;
}
