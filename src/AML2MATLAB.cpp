#include "AML2MATLAB.h"

AML2MATLAB::AML2MATLAB(AMLDOMParser *p, const std::string &path)
{
    parser = p;
    script.open(path);

    simulinkPaths.open("res/SimulinkPaths.json");
    simulinkAttributes.open("res/SimulinkAttributes.json");
    simulinkAttributeValues.open("res/SimulinkAttributeValues.json");
    simulinkPaths >> simulinkPathsJSON;
    simulinkAttributes >> simulinkAttributesJSON;
    simulinkAttributeValues >> simulinkAttributeValuesJSON;
}

AML2MATLAB::~AML2MATLAB()
{
    script.close();
}

void AML2MATLAB::convertToMATLAB()
{
    // writing a brief comment and the model parameters
    writeHeaderComment();

    // get the special Model and Interface InternalElement
    InternalElement model, interfaceInputs, interfaceOutputs, simulationControl;
    for (auto &ie : parser->getInternalElements())
    {
        if (ie.name == u"Model")
            model = ie;
        if (ie.name == u"Interface")
            for (auto &t : ie.internalElements)
            {
                if (t.name == u"Input")
                    interfaceInputs = t;
                if (t.name == u"Output")
                    interfaceOutputs = t;
            }
        if (ie.name == u"Simulation Control")
            simulationControl = ie;
    }

    writeModelParams(simulationControl);

    for (auto &ie : model.internalElements)
        writeAddBlock(ie);
    for (auto &ie : interfaceInputs.internalElements)
        writeAddBlock(ie);
    for (auto &ie : interfaceOutputs.internalElements)
        writeAddBlock(ie);

    for (auto &ie : parser->getInternalElements())
        writeAddLine(ie);
    for (auto &ie : model.internalElements)
        writeAddLine(ie);
    for (auto &ie : interfaceInputs.internalElements)
        writeAddLine(ie);
    for (auto &ie : interfaceOutputs.internalElements)
        writeAddLine(ie);

    writeEnd();
}

void AML2MATLAB::writeHeaderComment()
{
    script << "%============================================="
              "===================="
              "================="
           << std::endl;
    script << "% The "
           << toNative(dynamic_cast<DOMElement *>(
                           parser->getDOMDocument()->getElementsByTagName(AMLInstanceHierarchy)->item(0))
                           ->getAttribute(AMLName))
           << " model" << std::endl;
    script << "% This script was generated automatically by "
              "the AMLparser "
              "(https://gitlab.com/JackHack96/amlparser)"
           << std::endl;
    script << "%============================================="
              "===================="
              "================="
           << std::endl
           << std::endl;
}

void AML2MATLAB::writeModelParams(const InternalElement &block)
{
    script << "model = '"
           << toNative(dynamic_cast<DOMElement *>(
                           parser->getDOMDocument()->getElementsByTagName(AMLInstanceHierarchy)->item(0))
                           ->getAttribute(AMLName))
           << "';" << std::endl;
    script << "new_system(model);" << std::endl;
    script << "open_system(model);" << std::endl;
    for (auto &attribute : block.attributeList)
    {
        script << "set_param(model,'" << attribute.first << "','" << attribute.second << "');" << std::endl;
    }
}

void AML2MATLAB::writeAddBlock(const InternalElement &block)
{
    script << "add_block('" << getSimulinkPath(block.refBaseRoleClassPath) << "', [model, '/" << block.name << "']";
    for (auto &a : block.attributeList)
    {
        script << ",...\n    '" << getSimulinkAttributeName(block.refBaseRoleClassPath, a.first) << "', '"
               << getSimulinkAttributeValue(block.refBaseRoleClassPath, a.second) << "'";
    }
    script << ");" << std::endl;
}

void AML2MATLAB::writeAddLine(const InternalElement &block)
{
    for (auto &i : block.internalLinks)
    {
        // get the two referenced InternalElements
        auto source = parser->getInternalElementById(i.first.substr(0, i.first.find(u":")));
        auto dest   = parser->getInternalElementById(i.second.substr(0, i.second.find(u":")));

        // get the real port names, stored in the SimulinkName
        // attribute
        XercesString sourcePort, destPort;
        for (auto &j : source->extnInterfaces)
        {
            if (j.name == i.first.substr(i.first.find(u":") + 1, i.first.length()))
                sourcePort = j.simulinkName;
        }
        for (auto &j : dest->extnInterfaces)
        {
            if (j.name == i.second.substr(i.second.find(u":") + 1, i.second.length()))
                destPort = j.simulinkName;
        }

        // do the actual write
        script << "add_line(model, '" << source->name << "/" << sourcePort << "', '" << dest->name << "/" << destPort
               << "');" << std::endl;
    }
}

void AML2MATLAB::writeEnd()
{
    script << "Simulink.BlockDiagram.arrangeSystem(model);" << std::endl;
    script << "exportToFMU2CS(model, ...\n"
              "    'CreateModelAfterGeneratingFMU', 'off', ...\n"
              "    'AddIcon', 'snapshot', ...\n"
              "    'SaveDirectory', pwd);";
}

std::string AML2MATLAB::getSimulinkPath(const XercesString &path)
{
    if (simulinkPathsJSON.contains(toNative(path)))
        return simulinkPathsJSON[toNative(path)];
    else
        return toNative(path);
}

std::string AML2MATLAB::getSimulinkAttributeName(const XercesString &path, const XercesString &attributeName)
{
    if (simulinkAttributesJSON.contains(toNative(path)))
    {
        if (simulinkAttributesJSON[toNative(path)].contains(toNative(attributeName)))
            return simulinkAttributesJSON[toNative(path)][toNative(attributeName)];
        else
            return toNative(attributeName);
    }
    else
        return toNative(attributeName);
}

std::string AML2MATLAB::getSimulinkAttributeValue(const XercesString &path, const XercesString &attributeValue)
{
    if (simulinkAttributeValuesJSON.contains(toNative(path)))
    {
        if (simulinkAttributeValuesJSON[toNative(path)].contains(toNative(attributeValue)))
            return simulinkAttributeValuesJSON[toNative(path)][toNative(attributeValue)];
        else
            return toNative(attributeValue);
    }
    else
        return toNative(attributeValue);
}
