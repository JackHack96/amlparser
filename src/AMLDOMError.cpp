#include "AMLDOMError.h"

AMLDOMErrorHandler::AMLDOMErrorHandler()
{
    sawErrors = false;
}

AMLDOMErrorHandler::~AMLDOMErrorHandler() noexcept = default;

bool AMLDOMErrorHandler::getSawErrors() const
{
    return sawErrors;
}

bool AMLDOMErrorHandler::handleError(const DOMError &domError)
{
    sawErrors = true;
    if (domError.getSeverity() == DOMError::DOM_SEVERITY_WARNING)
        std::cerr << "\nWarning at file ";
    else if (domError.getSeverity() == DOMError::DOM_SEVERITY_ERROR)
        std::cerr << "\nError at file ";
    else
        std::cerr << "\nFatal Error at file ";

    std::cerr << toNative(domError.getLocation()->getURI()) << ", line " << domError.getLocation()->getLineNumber()
              << ", char " << domError.getLocation()->getColumnNumber()
              << "\n  Message: " << toNative(domError.getMessage()) << std::endl;

    return true;
}

void AMLDOMErrorHandler::resetErrors()
{
    sawErrors = false;
}
