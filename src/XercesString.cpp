#include "XercesString.h"

// convert from narrow character to wide character
inline XercesString fromNative(const char *str)
{
    std::unique_ptr<XMLCh> ptr(xercesc::XMLString::transcode(str));
    return XercesString(ptr.get());
}

XercesString fromNative(const std::string &str)
{
    return fromNative(str.c_str());
}

// convert from wide character to narrow character
inline std::string toNative(const XMLCh *str)
{
    std::unique_ptr<char> ptr(xercesc::XMLString::transcode(str));
    return std::string(ptr.get());
}

std::string toNative(const XercesString &str)
{
    return toNative(str.c_str());
}